package controllers

import play.api._
import play.api.mvc._
import play.api.Play.current
import com.typesafe.plugin.RedisPlugin

object Application extends Controller {

  def index = Action {

    val pool = current.plugin[RedisPlugin].getOrElse(throw new Exception("The Redis plugin was not registered or disabled.")).sedisPool
    pool.withClient { client =>
      client.incr("foo")
      client.expire("foo", 10)
    }

    Ok(views.html.index("Your new application is ready."))
  }

}